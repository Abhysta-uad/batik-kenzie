<ul class="breadcrumbs">
    <li class="nav-home">
        <a href="<?= base_url('admin')?>">
            <i class="flaticon-home"></i>
        </a>
    </li>
    <li class="separator">
        <i class="flaticon-right-arrow"></i>
    </li>
    <li class="nav-item">
        <a href="<?=  base_url('product')?>"><?= $title?></a>
    </li>
</ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= $title?></h4>
            </div>
            <div class="card-body">
                <a href="<?= base_url('product/addproduct')?>" type="button" class="btn btn-primary mb-2">Tambah
                    Product<i class="ml-2 fas fa-fw fa-plus"></i>
                </a>

                <?php if($this->session->flashdata('barang')) {
                    echo '<div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Success!</h5>';
                    echo $this->session->flashdata('barang');
                    echo '</div>';
                }?>

                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover">
                        <thead class="text-center">
                            <tr>
                                <th>No</th>
                                <th>Nama Product</th>
                                <th>Kategori</th>
                                <th>Stock</th>
                                <th>Harga</th>
                                <th>Detail</th>
                                <th>Gambar</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1;
                        foreach ($product as $p => $value) { ?>
                            <tr>
                                <th><?= $no++ ?></th>
                                <th><?= $value->nama_barang ?></th>
                                <th><?= $value->nama_kategori ?></th>
                                <th><?= $value->stock_barang ?></th>
                                <th>Rp. <?= number_format($value->harga_barang, 0) ?></th>
                                <th><?= $value->detail ?></th>
                                <th><img src="<?= base_url('assets/product/'. $value->gambar)?>"
                                        class="img-fluid rounded elevation-2">
                                </th>
                                <th>
                                    <a href="<?= base_url('product/edit/'. $value->id_barang)?>"
                                        class="btn btn-warning btn-sm m-1"><i class="fas fa-edit"></i></a>
                                    <button class="btn btn-danger btn-sm m-1" data-toggle="modal"
                                        data-target="#hapus<?= $value->id_barang ?>"><i
                                            class="fas fa-trash-alt"></i></button>
                                </th>
                            </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal Delete -->
<?php foreach ($product as $c => $value) { ?>
<div class="modal fade" id="hapus<?= $value->id_barang ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete <?= $value->nama_barang?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Apakah Anda Yakin Ingin Menghapus Product Ini ?</h6>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="<?= base_url('product/delete/'. $value->id_barang) ?>" class="btn btn-primary">Delete</a>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>