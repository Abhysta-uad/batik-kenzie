<ul class="breadcrumbs">
    <li class="nav-home">
        <a href="<?= base_url('admin')?>">
            <i class="flaticon-home"></i>
        </a>
    </li>
    <li class="separator">
        <i class="flaticon-right-arrow"></i>
    </li>
    <li class="nav-item">
        <a href="<?=  base_url('product')?>"><?= $title1?></a>
    </li>
    <li class="separator">
        <i class="flaticon-right-arrow"></i>
    </li>
    <li class="nav-item">
        <a href="#"><?= $title2?></a>
    </li>
</ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title"><?= $title2?></div>
            </div>
            <div class="card-body">
                <?php 
                
                echo validation_errors('<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h5><i class="icon fas fa-ban"></i>','</h5></div>');
                
                if (isset($error_up)) {
                    echo '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h5><i class="icon fas fa-ban"></i>'.$error_up.'</h5></div>';
                };

                echo form_open_multipart('product/addproduct') ?>

                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input name="nama_barang" class="form-control" placeholder="Nama Barang"
                                value="<?= set_value('nama_barang')  ?>">

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="id_kategori" class="form-control">
                                <option value="">--Pilih Kategori--</option>
                                <?php foreach ($categories as $c => $value) { ?>
                                <option value="<?= $value->id_kategori ?>"><?= $value->nama_kategori ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Stock Barang</label>
                            <input name="stock_barang" class="form-control" placeholder="Stock Barang"
                                value="<?= set_value('stock_barang')  ?>">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Harga Barang</label>
                            <input name="harga_barang" class="form-control" placeholder="Harga Barang"
                                value="<?= set_value('harga_barang')  ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Detail Barang</label>
                    <textarea name="detail" class="form-control" rows="10"
                        placeholder="Detail Barang"><?= set_value('detail')?></textarea>
                </div>

                <div class="form-group">
                    <label>Gambar Barang</label>
                    <input type="file" name="gambar" class="form-control" id="preview_gambar" required>
                </div>

                <div class="form-group">
                    <img src="" id="gambar_load" class="img-fluid">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-md">Simpan Product</button>
                    <a href="<?= base_url('product') ?>" class="btn btn-danger btn-md">Kembali</a>
                </div>

                <?= form_close() ?>

            </div>
        </div>
    </div>
</div>