<ul class="breadcrumbs">
    <li class="nav-home">
        <a href="<?= base_url('admin')?>">
            <i class="flaticon-home"></i>
        </a>
    </li>
    <li class="separator">
        <i class="flaticon-right-arrow"></i>
    </li>
    <li class="nav-item">
        <a href="<?=  base_url('usercrud')?>"><?= $title?></a>
    </li>
</ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Basic</h4>
            </div>
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#tambah">Tambah
                    User<i class="ml-2 fas fa-fw fa-plus"></i>
                </button>

                <?php if($this->session->flashdata('usr')) {
                    echo '<div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Success!</h5>';
                    echo $this->session->flashdata('usr');
                    echo '</div>';
                }?>

                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover text-center">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Role</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1;
                        foreach ($ucrud as $u => $value) { ?>
                            <tr>
                                <th><?= $no++ ?></th>
                                <th class="text-break"><?= $value->name ?></th>
                                <th class="text-break"><?= $value->email ?></th>
                                <th class="text-break"><?= $value->password ?></th>
                                <th><?php if ($value->role_id==1) {
                                echo '<span class="badge badge-pill badge-warning">Admin</span>';
                            } else {
                                echo '<span class="badge badge-pill badge-primary">Member</span>';
                            }
                            ?></th>
                                <th><?php if ($value->is_active==1) {
                                echo '<span class="badge badge-pill badge-success">Active</span>';
                            } else {
                                echo '<span class="badge badge-pill badge-danger">No</span>';
                            }
                            ?></th>
                                <th>
                                    <button class="btn btn-warning btn-sm m-1" data-toggle="modal"
                                        data-target="#edit<?= $value->id_user ?>"><i class="fas fa-edit"></i></button>
                                    <button class="btn btn-danger btn-sm m-1" data-toggle="modal"
                                        data-target="#hapus<?= $value->id_user ?>"><i
                                            class="fas fa-trash-alt"></i></button>
                                </th>
                            </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('usercrud/add') ?>

                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter username" required>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Enter email" required>
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Enter password" required>
                </div>

                <div class="form-group">
                    <label>Role</label>
                    <select name="role_id" class="form-control">
                        <option value="1">Admin</option>
                        <option value="2" selected>Member</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Active</label>
                    <select name="is_active" class="form-control">
                        <option value="0" selected>Not Active</option>
                        <option value="1">Active</option>
                    </select>
                </div>

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?php echo form_close() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- modal edit -->
<?php foreach ($ucrud as $u => $value) { ?>
<div class="modal fade" id="edit<?= $value->id_user ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('usercrud/edit/'. $value->id_user) ?>

                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="name" value="<?= $value->name ?>" class="form-control"
                        placeholder="Enter username" required>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" value="<?= $value->email ?>" class="form-control"
                        placeholder="Enter email" required>
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value="<?= $value->password ?>" class="form-control"
                        placeholder="Enter password" required>
                </div>

                <div class="form-group">
                    <label>Role</label>
                    <select name="role_id" class="form-control">
                        <option value="1" <?php if($value->role_id==1){echo 'selected';} ?>>Admin</option>
                        <option value="2" <?php if($value->role_id==2){echo 'selected';} ?>>Member</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Active</label>
                    <select name="is_active" class="form-control">
                        <option value="0" <?php if($value->is_active==0){echo 'selected';} ?>>Not Active</option>
                        <option value="1" <?php if($value->is_active==1){echo 'selected';} ?>>Active</option>
                    </select>
                </div>

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?php echo form_close() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>



<!-- modal Delete -->
<?php foreach ($ucrud as $u => $value) { ?>
<div class="modal fade" id="hapus<?= $value->id_user ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete <?= $value->name?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Apakah Anda Yakin Ingin Menghapus User Ini ?</h6>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="<?= base_url('usercrud/delete/'. $value->id_user) ?>" class="btn btn-primary">Delete</a>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>