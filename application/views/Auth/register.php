<section>
    <div class="container py-5 mt-2">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-8 col-lg-7 col-xl-6">
                <img src="<?= base_url()?>/assets/1tes.png" class="img-fluid" alt="Phone image">
            </div>
            <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1 card shadow p-3">
                <form class="user" method="POST" action="<?= base_url('auth/register')?>">
                    <!-- Title -->
                    <div class="card-body">
                        <div class="mb-3 pb-1 card-title">
                            <i class='bx bxs-shopping-bags h2' style="color: #5faaff;"></i>
                            <span class="h1 fw-bold mb-0">Register</span>
                        </div>

                        <!-- Username Input -->
                        <div class="form-outline mb-2">
                            <label class="form-label">Username</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class='bx bxs-user'></i></div>
                                </div>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Username"
                                    value="<?= set_value('name');?>">

                            </div>
                            <?php echo form_error('name','<small class="text-danger pl">','</small>') ?>
                        </div>

                        <!-- Email input -->
                        <div class="form-outline mb-2">
                            <label class="form-label">Email address</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class='bx bxs-envelope'></i></div>
                                </div>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email"
                                    value="<?= set_value('email');?>">

                            </div>
                            <?php echo form_error('email','<small class="text-danger">','</small>') ?>
                        </div>

                        <!-- Password1 input -->
                        <div class="row">
                            <div class="col">

                                <div class="form-outline mb-2">
                                    <label class="form-label">Password</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class='bx bxs-lock'></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password1" name="password1"
                                            placeholder="Password">

                                    </div>
                                    <?php echo form_error('password1','<small class="text-danger">','</small>') ?>
                                </div>
                            </div>

                            <!-- Password2 input -->
                            <div class="col">
                                <div class="form-outline mb-2">
                                    <label class="form-label">Password</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class='bx bxs-lock'></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="password2" name="password2"
                                            placeholder="Password">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="d-flex justify-content-betweem align-items-left mb-4">
                            <a href="#!">Forgot password?</a>
                        </div>

                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Sign Up</button>

                        <div class="text-center text-lg-start mt-2 pt-2">
                            <p class="small fw-bold mt-2 pt-1 mb-0">Sudah Memiliki Akun ? <a href="<?= base_url()?>"
                                    class="link-danger">Login</a></p>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>