<div class="card">
    <div class="card-body">
        <?php echo form_open('keranjang/update'); ?>

        <table class="table" style="width:100%">

            <tr>
                <th width="20px">Jumlah_Barang</th>
                <th>Nama Barang</th>
                <th style="text-align:center">Harga</th>
                <th style="text-align:center">Sub-Total</th>
                <th>Action</th>
            </tr>

            <?php $i = 1; ?>

            <?php foreach ($this->cart->contents() as $items): ?>

            <tr>
                <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5', 'type'=>'number', 'class'=>'text-center form-control', 'min'=>'0')); ?>
                </td>
                <td>
                    <?php echo $items['name']; ?>
                </td>
                <td style="text-align:center">Rp. <?php echo number_format($items['price'], 0); ?></td>
                <td style="text-align:center">Rp. <?php echo number_format($items['subtotal'], 0); ?></td>
                <td class="text center">
                    <a href="<?= base_url('keranjang/delete/'. $items['rowid'])?>" class="btn btn-danger btn-sm m-1"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>

            <?php $i++; ?>

            <?php endforeach; ?>

            <tr>
                <td colspan="2"> </td>
                <td class="right" style="text-align:center"><strong>
                        <h4>Harga Total :</h4>
                    </strong></td>
                <td class="right" style="text-align:center"><strong>
                        <h4>Rp. <?php echo number_format($this->cart->total(), 0); ?></h4>
                    </strong></td>
                <td></td>
            </tr>

        </table>

        <button type="submit" class="btn btn-primary">Update Keranjang</button>
        <a href="<?= base_url('keranjang/deleteall')?>" class="btn btn-danger">Hapus Semua</a>
        <a href="<?= base_url('keranjang/pembayaran')?>" class="btn btn-success">Check Out</a>

        <?php echo form_close();?>
    </div>
</div>