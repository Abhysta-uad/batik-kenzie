<!-- Default box -->
<div class="card card-solid">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h3 class="d-inline-block d-sm-none"><?= $product->nama_barang?></h3>
                <div class="col-12">
                    <img src="<?= base_url('assets/product/'. $product->gambar);?>" class="product-image"
                        alt="Product Image">
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <h3 class="my-3"><?= $product->nama_barang?></h3>
                <p><?= $product->nama_kategori?></p>

                <hr>

                <div class="bg-gray py-2 px-3 mt-4">
                    <h2 class="mb-0">
                        Rp. <?= number_format($product->harga_barang, 0)?>
                    </h2>
                </div>

                <?php
                echo form_open('keranjang/add');
                echo form_hidden('id', $product->id_barang);
                echo form_hidden('price', $product->harga_barang);
                echo form_hidden('name', $product->nama_barang);
                echo form_hidden('redirect_page', str_replace('index.php/', '', current_url()));
                ?>
                <div class="mt-4">
                    <div class="row">
                        <div class="col-sm-2">
                            <input type="number" class="form-control" name="qty" value="1" min="1">
                        </div>
                        <div class="col-sm-8">
                            <button class="btn btn-primary btn-flat toastrDefaultSuccess">
                                <i class="fas fa-cart-plus fa-lg mr-2"></i>
                                Add to Cart
                            </button>
                        </div>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
        <div class="row mt-4">
            <nav class="w-100">
                <div class="nav nav-tabs" id="product-tab" role="tablist">
                    <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc"
                        role="tab" aria-controls="product-desc" aria-selected="true">Description</a>
                </div>
            </nav>
            <div class="tab-content p-3" id="nav-tabContent">
                <div class="tab-pane fade show active" id="product-desc" role="tabpanel"
                    aria-labelledby="product-desc-tab"><?= $product->detail?></div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->