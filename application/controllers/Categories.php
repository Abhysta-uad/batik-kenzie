<?php 




defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('model_categories');
    }

    // List all your items
    public function index( $offset = 0 )
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Halaman Administrator';
        $data ['title']= 'Categories';
        $data['categories'] = $this->model_categories->get_categories();

        $this->load->view('template_admin/admin_header', $data);
        $this->load->view('admin_view/index');
        $this->load->view('admin_view/content_categories');
        $this->load->view('template_admin/admin_footer');
    }

    // Add a new item
    public function add()
    {
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
         );
         $this->model_categories->add($data);
         $this->session->set_flashdata('kategori', 'Data Berhasil Di Tambahkan');
         redirect('categories');
    }

    //Update one item
    public function edit( $id_kategori = NULL )
    {
        $data = array(
            'id_kategori' => $id_kategori,
            'nama_kategori' => $this->input->post('nama_kategori'),
         );
         $this->model_categories->edit($data);
         $this->session->set_flashdata('kategori', 'Data Berhasil Di Edit');
         redirect('categories');
    }

    //Delete one item
    public function delete( $id_kategori = NULL )
    {
        $data = array('id_kategori' => $id_kategori );
        $this->model_categories->delete($data);
        $this->session->set_flashdata('kategori', 'Data Berhasil Di Hapus');
         redirect('categories');
    }
}

/* End of file Controllername.php */




?>