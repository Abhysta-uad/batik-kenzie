<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Model_product extends CI_Model {

    public function get_product(){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->join('categories', 'categories.id_kategori = product.id_kategori', 'left');
        
        $this->db->order_by('id_barang', 'desc');
        return $this->db->get()->result();   
    }

    public function get_data($id_barang){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->join('categories', 'categories.id_kategori = product.id_kategori', 'left');
        $this->db->where('id_barang', $id_barang);
        
        return $this->db->get()->row();
    }

    public function add($data){
        $this->db->insert('product', $data);
    }

    public function edit($data){
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->update('product', $data);
    }

    public function delete($data){
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->delete('product', $data);
    }

}

/* End of file ModelName.php */


?>