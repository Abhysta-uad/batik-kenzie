<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

    public function get_product(){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->join('categories', 'categories.id_kategori = product.id_kategori', 'left');
        
        $this->db->order_by('id_barang', 'desc');
        return $this->db->get()->result();   
    }

    public function detail($id_barang){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->join('categories', 'categories.id_kategori = product.id_kategori', 'left');
        
        $this->db->where('id_barang', $id_barang);
        return $this->db->get()->row();  
    }
}

/* End of file Model_user.php */
?>